[]
SOUNDCUE#StartNightSound
SOUNDCUE#StartDinnerSound
Simon#Another day done.
Eric#Dad, is the fence going to break again like last night?
Simon#Of course not, pumpkin. Mama will build an extra strong one that'll last a very long time. Right, honey?
Mother#Mmhm.
Emma#What kind of thing does that though? Those footprints…
Simon#It's nothing you need to worry about. Everything will be fine. Let's just focus on shearing the sheep.
Lisa#Tell that to the monsters….
Emma#I'm with squirt on this one. Maya's family has a house in town, we could-
Simon#No. 
Emma#But dad-
Simon#I said no. This is our home, you understand? We have to protect it. 
Emma#….Who said protecting this place is worth it?
Simon#Leave my table.
Emma#What?
Eric#Papa?
Simon#I said, leave my table! You can go to bed without your dinner.
Emma#I-fine! Whatever! I'm not some kid you can bully around!
Eric#Hey!
Lisa#Sis?
Emma#Food#We've got plenty of food, no thanks to you!
Emma#NoFood#It's not like we were going to have much food to go around anyway, squirt. You take my plate.
Lisa#But, Sis…
Eric#Mama?
Mother#…
Simon#What is wrong with that child?
SOUNDCUE#StopDinnerSound
SOUNDCUE#StartMonsterSound
PAUSE#1
Monster#Thht…
Monster#Thht…
Mother#…
Monster#Perhaps we were unclear.
Monster#Betrayal is coming, mother.
Monster#For now, we will sate ourselves on your flock.
Monster#Pick the wool from out teeth.
Monster#Thht…
SOUNDCUE#StopAllSound
[eric]
SOUNDCUE#StartNightSound
SOUNDCUE#StartDinnerSound
Simon#Where's Eric?
Emma#He's scared of the dark.
Lisa#But you're not scared of anything, are you?
Emma#Hah, squirt, don't you worry about me…
Lisa#Anyway, he deserves to be lost at night, he's so weird lately!
Emma#I hope he's just lost…
Lisa#Can we eat now?
Emma#Lis, come on…
Simon#No, no, she's right, there's nothing to be concerned over so we should just act normal.
Emma#Food#All right, well, tonight we have, um. Beets.
Emma#NoFood#Right, well, I … I got some beets.
Simon#NoFood#Emma, where did you get those?
Emma#NoFood#I didn't steal them, if that's what you're asking!
Simon#NoFood#I would never… I'm sorry, sweet pea, thank you.
Lisa#What are beets?
Simon#Sort of a root … well, I'm sure you'll love them! Let's make a nice hot borscht and pretend we're in the snowy North.
Lisa#Brrr!
Simon#That's the spirit! Go on, Emma, you try. 
Emma#Uh… heh… brr…
Simon#Dearest?
Mother#…
SOUNDCUE#StopDinnerSound
SOUNDCUE#StartMonsterSound
PAUSE#1
Mother#…
Monster#Thht…
Monster#Gold is your reward.
Monster#A full belly as well….
Monster#We will return tomorrow.
Monster#To pick at your bones, once again.
SOUNDCUE#StopAllSound
[emma]
SOUNDCUE#StartNightSound
SOUNDCUE#StartDinnerSound
Simon#Where's Emma? Is she sulking? That girl… Well, she can always come inside if she needs to.
Lisa#I should be out with her!
Eric#Me too!
Lisa#No! You can't do anything, I can help her!
Eric#I can, I can!
Simon#Come on you two, she's not in any danger. She's right outside!
Lisa#Yeah, but…
Simon#Your mother and I would never send you into any danger, promise. 
Mother#Mm…
Eric#Mama?
Simon#NoFood# Now who's ready for… where did these beets come from?
Mother#NoFood# Hm?
Simon#NoFood# I mean, we'll have a nice borscht for dinner tonight!
Simon#Food#Now, who's ready for dinner? 
Eric#Food#I am!
Lisa#Food#Are you sure there's enough, I don't mind…
Simon#Food#We have plenty, your mother's been working very hard. 
Eric#Food#Thanks, Mama!
Lisa#Food#Cut it out. Mama likes me best, anyhow. 
Eric#Food#No she doesn't!
Simon#Food#We love you both very much. Now, let's eat!
SOUNDCUE#StopDinnerSound
SOUNDCUE#StartMonsterSound
PAUSE#1
Mother#…
Monster#Thht…
Monster#Gold is your reward.
Monster#A full belly as well….
Monster#We will return tomorrow.
Monster#To pick at your bones … again.
SOUNDCUE#StopAllSound
[lisa]
SOUNDCUE#StartNightSound
SOUNDCUE#StartDinnerSound
Simon#Was that really necessary? 
Mother#Mmhm.
Simon#Well, if you say so. Some strange things are happening out there, she can always come inside if she needs to.
Emma#Just so long as she doesn't run off.
Eric#She'd never run! 
Emma#…That's kind of what I'm worried about.
Simon#Besides, this isn't a punishment, it's a responsibility. We all have to do our part.
Emma#You're doing what you can, Dad. 
Simon#Oh, I know… haha…
Eric#Let's eat, let's eat!
Emma#Stop shouting!
Simon#No, he's right. There's nothing to be worried about so we should just … act normal.
Emma#Food#Fine. Well, tonight we have … beets.
Emma#NoFood#Right, well, I um…. I got these beets.
Simon#NoFood#Where did you get those?
Emma#NoFood#I didn't steal them, if that's what you're asking!
Simon#NoFood#I would never… I'm sorry, I'm just worried about Lisa. Thank you.
Eric#I've never had beets before…
Simon#Well, I'm sure you'll love them! Let's make a nice hot borscht and pretend we're in the snowy North.
Eric#Brrr!
Simon#That's the spirit! Go on, Emma.
Emma#I… ugh. No.
Simon#Dearest?
Mother#…
SOUNDCUE#StopDinnerSound
SOUNDCUE#StartMonsterSound
PAUSE#1
Mother#…
Monster#Thht…
Monster#Gold is your reward.
Monster#A full belly as well….
Monster#We will return tomorrow.
Monster#Betrayal is coming.
SOUNDCUE#StopAllSound
