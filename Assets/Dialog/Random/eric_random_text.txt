1#It’s so nice out today!
1#I love the sheep, mama!
1#I found some weird bugs in the field, wanna see?
1#The sheep smell kind of funny.
1#When’s lunch?
2#I’m hungry, Mama…
2#Tell Lisa to stop hitting me!
2#I heard something last night…
2#Lisa! Stop it!
2#I have a new pet spider. His name is Bitey. He bites.
2#Mama, you shouldn’t run with scissors.
3#Did you hear that?
3#Is there something wrong, Mama?
3#Dad looks really sad.
3#I ate some hay, it tastes really bad!
3#Bitey bit me!
3#It’s really, really cold.
3#Emma told me that there’s something bad in the woods…
4#Is something wrong?
4#You look tired, Mama. Can I help?
4#The sheep are really loud today…
4#I’ll protect you, Mama.
4#What do you think keeps breaking the fences, Mama?
5#Where’s Emma, Mama?#emmaKilled
5#Where’s Lisa, Mama?#lisaKilled
5#Can I help build the fences?
5#The wool really hurts…
5#I found some wool under my pillow, am I going to turn into a sheep?
5#Lisa’s mean to me!#lisaAlive
5#I’m going to trick Emma. Don’t tell her!#emmaAlive
5#Sometimes the sheep talk!
6#…
6#Do you love me?#AnyoneKilled
6#I don’t like this, Mama.
6#We should go away.
6#Papa says everything will be okay…
6#I’m sorry, Emma, I didn’t mean it…#emmakilled
6#Sis, is … that you?#lisakilled
6#I'm scared.
6#Do you hate me, mama?#AnyoneKilled
7#I want to be with them!#AnyoneKilled
7#I’m happy we’re together.#NoOneKilled
7#Please!#AnyoneKilled
7#Let me go!#AnyoneKilled
7#I hate you, mama!#AnyoneKilled
7#Papa, please!#AnyoneKilled
7#The sheep are quiet today.#NoOneKilled
7#The forest isn’t loud anymore…#NoOneKilled
7#I love you, mama.#NoOneKilled
7#We’re all going to town tomorrow, right?#NoOneKilled
7#Sunday’s tomorrow!#NoOneKilled
1,2,3#Can I help?
1,2,3#I want to shear the sheep!
1,2,3#When’s dinner?
1,2,3,4,5,6#What’s in the woods, mama?
3,4,5#I saw a train in town! Please, Mama?#ericnotoy
3,4,5#I want the train!#ericnotoy
2,3,4,5,6#I’m hungry.#nofood
3,4,5,6#These new clothes are so warm!#ericclothes
1,2,3,4,5,6#I’m cold.#ericNoClothes
3,4,5#Choo choo!#erictoy
3,4,5#Oh no! The train is crashing, everyone’s dead!#erictoy
4,5,6#Why are you so quiet today, mama?
1,2,3,4,5,6#I love you, mama.
