﻿public class TeenToy : ItemBase
{
    public override int Price
    {
        get
        {
            return 10;
        }
    }

    public override string Description
    {
        get
        {
            return "I really shouldn’t be selling this… but, I know you’ll really like it, Emma.";
        }
    }

    public override void Action()
    {
        GameManager.SetCondition("emmatoy", true);
        GameManager.SetCondition("emmanotoy", false);
    }
}
