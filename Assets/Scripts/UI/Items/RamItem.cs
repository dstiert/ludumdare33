﻿public class RamItem : ItemBase
{
    public override int Price
    {
        get
        {
            return 10 + GameManager.RamPurchased * 5;
        }
    }

    public override string Description
    {
        get
        {
            return "These rams are breaking everything in the shop. They’re really tough, so maybe you could use them in your fields? Just remember, they’re going to get more expensive the more you buy.";
        }
    }

    protected override bool Single { get { return false; } }

    public override void Action()
    {
        GameManager.RamPurchased++;
        GameManager.SpawnAnimal(GameManager.Ram);
    }
}
