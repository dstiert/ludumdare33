﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Bell : MonoBehaviour, IPointerClickHandler
{
    private GameManager _gameManager;
    private Animator _animator;
    private AudioSource _audio;

    public void Start ()
    {
        _gameManager = this.GetComponentInParent<GameManager>();
        _animator = this.GetComponent<Animator>();
        _audio = this.GetComponent<AudioSource>();
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        _audio.Play();
        _animator.SetTrigger("Ring");
        _gameManager.RingBell();
    }
}
