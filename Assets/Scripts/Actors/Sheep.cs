﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Sheep : CharacterBase, IPointerClickHandler
{
    public int Value;

    public int WoolValue;

    public int KillOrder;

    private Animator _animator;

    private Animator _scoreAnimator;

    private GameManager _gameManager;

    private SpriteRenderer _mark;

    private BoxCollider2D _collider;

    private float _waitTill;

    private Transform _scoreTransform;

    private TextMesh ScoreText;

    private SheepState _state;
    public SheepState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            _animator.SetInteger("State", (int)value);

            switch(value)
            {
                case SheepState.Wool:
                    _collider.enabled = true;
                    Wander();
                    break;
                case SheepState.Sheared:
                    _collider.enabled = false;
                    _mark.enabled = false;
                    var score = WoolValue + _gameManager.BonusScore;
                    ScoreText.text = "+" + score.ToString();
                    _scoreAnimator.SetTrigger("Score");
                    _gameManager.AddMoney(score);
                    break;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (State == SheepState.Wool)
        {
            if (_gameManager.Mother.Target != null)
            {
                _gameManager.Mother.Target.State = SheepState.Wool;
                _gameManager.Mother.Target.Wander();
                _gameManager.Mother.Target = null;
                _gameManager.Mother.State = MotherState.Idle;
            }

            _gameManager.Mother.Shear(this);
            _mark.enabled = true;
            Freeze();
            _waitTill = float.MaxValue;
        }
    }

    public override void Start()
    {
        leftFacing = true;
        base.Start();
        _collider = this.GetComponent<BoxCollider2D>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _mark = this.transform.Find("Mark").GetComponent<SpriteRenderer>();
        _mark.enabled = false;
        _animator = this.transform.Find("Sprite").GetComponent<Animator>();
        _animator.speed = Random.Range(0.75f, 1.25f);
        _scoreTransform = this.transform.Find("Score");
        _scoreAnimator = _scoreTransform.GetComponent<Animator>();
        ScoreText = this.GetComponentInChildren<TextMesh>();
        Wander();
	}

    public void Wander()
    {
        _mark.enabled = false;
        _waitTill = Time.time + Random.Range(2f, 8f);
    }

    public override void Update()
    {
        base.Update();

        _scoreTransform.localScale = new Vector3(this.transform.localScale.x > 0 ? 1f : -1f, 1f, 1f);

        if (_waitTill < Time.time)
        {
            _waitTill = Time.time + Random.Range(2f, 8f);
            this.GoTo(_gameManager.RandomFieldPoint());
        }
	}
}
