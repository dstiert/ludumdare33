﻿using UnityEngine;
using UnityEngine.UI;

public class Title : MonoBehaviour
{	
	private CanvasGroup _canvas;

	public void Start()
	{
		_canvas = this.GetComponent<CanvasGroup>();
	}

	public void Update()
	{
		if (Time.time > 4f) {
			_canvas.alpha = _canvas.alpha - Time.deltaTime / 2f;
			if (Mathf.Approximately (_canvas.alpha, 0f)) {
				Destroy (this.gameObject);
			}
		}
	}
}
