﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Globalization;

public class GameManager : MonoBehaviour, IPointerClickHandler
{
    public AudioClip DayAmbient;

    public AudioClip NightAmbient;

    private AudioSource _audio;

    public Sprite Monster;

    public Texture2D CursorTexture;

    public Vector2 CursorHotspot;

    public int Day = 1;

    public List<TextAsset> NightDialogs;

    private Dictionary<string, bool> _conditions;

    public Mother Mother;

    public int Money = 50;

    private Text _moneyText;

    private Text _dayText;

    private ChooseBox ChooseBox;

    private DialogBox DialogBox;

    private CanvasGroup Night;

    private CanvasGroup NightMonster;

    public bool ShowMonster;

    private bool _night;

    private bool _transition;

    private string _sacrificeName;

    private Collider2D Collider;

    private List<FamilyMember> Family;

    private List<string> Dead;

    private int _familyStart;

    private List<Sheep> Sheeps;

    private Fence Fence;

    private Color _font;

    private Color _highlight;

    public GameObject BlackSheep;

    public GameObject Ram;

    public GameObject Sheep;

    public int FoodTier;

    public int BonusScore;

    public int SheepPurchased;

    public int RamPurchased;

    public bool Bell;

    public int AnimalCount
    {
        get { return Sheeps.Count; }
    }

    public void Start()
    {
        //Cursor.SetCursor(CursorTexture, CursorHotspot, CursorMode.Auto);
        _conditions = new Dictionary<string, bool>
        {
            { "nofood", false },
            { "emmaflive", true },
            { "emmafilled", false },
            { "emmaflothes", false },
            { "emmatoy", false },
            { "emmanoclothes", true },
            { "emmanotoy", true },
            { "ericalive", true },
            { "erickilled", false },
            { "ericclothes", false },
            { "erictoy", false },
            { "ericnoclothes", true },
            { "ericnotoy", true },
            { "lisaalive", true },
            { "lisakilled", false },
            { "lisaclothes", false },
            { "lisatoy", false },
            { "lisanoclothes", true },
            { "lisanotoy", true }
        };
        _moneyText = GameObject.Find("Money").GetComponent<Text>();
        _dayText = GameObject.Find("Day").GetComponent<Text>();
        Night = GameObject.Find("Night").GetComponent<CanvasGroup>();
        Fence = GameObject.Find("Fence").GetComponent<Fence>();
        Mother = GameObject.Find("Mother").GetComponent<Mother>();
        Collider = this.GetComponentInChildren<Collider2D>();
        ChooseBox = GameObject.Find("ChooseBox").GetComponent<ChooseBox>();
        DialogBox = GameObject.Find("DialogBox").GetComponent<DialogBox>();
        Family = GameObject.FindGameObjectsWithTag("Family").Select(go => go.GetComponent<FamilyMember>()).ToList();
        _familyStart = Family.Count;
        Sheeps = GameObject.FindGameObjectsWithTag("Sheep").Select(go => go.GetComponent<Sheep>()).ToList();
        Dead = new List<string>();
        NightMonster = GameObject.Find("NightMonster").GetComponent<CanvasGroup>();
        _audio = this.GetComponent<AudioSource>();
        FoodTier = 1;
        BonusScore = 0;
        SheepPurchased = 0;
        RamPurchased = 0;
        Bell = false;
    }

    public void ShowBell()
    {
        Bell = true;
        var bell = this.transform.Find("Bell");
        bell.GetComponent<SpriteRenderer>().enabled = true;
        bell.GetComponent<BoxCollider2D>().enabled = true;
    }

    public void Update()
    {
        var a = Night.alpha;
        if (_transition)
        {
            if (_night && _transition)
            {
                Night.alpha = Mathf.Clamp(a + Time.deltaTime / 4f, 0f, 1f);

                if (Mathf.Approximately(a, 1f))
                {
                    Night.alpha = 1f;
                    _transition = false;
                    ExecuteNight();
                }
            }
            else
            {
                Night.alpha = Mathf.Clamp(a - Time.deltaTime / 4f, 0f, 1f);

                if (Mathf.Approximately(a, 0f))
                {
                    Night.alpha = 0f;
                    _transition = false;
                }
            }
        }

        if (ShowMonster)
        {
            NightMonster.alpha = Mathf.Clamp(a + Time.deltaTime / 2f, 0f, 1f);
        }
        else
        {
            NightMonster.alpha = 0f;
        }

        _dayText.text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[Day - 1];
    }

    public bool GetCondition(string name)
    {
        bool ret;
        return _conditions.TryGetValue(name.ToLower(), out ret) && ret;
    }

    public void SetCondition(string name, bool state)
    {
        _conditions[name] = state;
    }

    public void AddMoney(int money)
    {
        Money += money;
        _moneyText.text = Money.ToString();
    }

    public void RemoveMoney(int money)
    {
        Money -= money;
        _moneyText.text = Money.ToString();
    }

    public Vector3 RandomFieldPoint()
    {
        var bounds = Collider.bounds;
        Vector2 point;
        do
        {
            point = new Vector2(Random.Range(bounds.min.x, bounds.max.x), Random.Range(bounds.min.y, bounds.max.y));
        } while (!Collider.OverlapPoint(point));

        return new Vector3(point.x, point.y, 0f);
    }

    public void RingBell()
    {
        List<Choice> options = new List<Choice>();
        if (Day >= 4)
        {
            options = Family.Where(f => f.Name != "Simon" || (Day == 7 && Family.Count == 1)).Select(f => new Choice
            {
                Text = string.Format("Keep the sheep safe tonight, {0}.", f.Name),
                Callback = () => BeginNight(f.Name)
            }).ToList();
        }

        options.Add(new Choice { Text = "Come inside, everyone.", Callback = () => BeginNight(string.Empty) });

        ChooseBox.ShowOptions(options);
    }

    public void BeginNight(string sacrificeName)
    {
        _sacrificeName = sacrificeName;
        _night = true;
        _transition = true;
    }

    public void EndNight()
    {
        SetCondition("nofood", true);
        ShowMonster = false;
        _night = false;
        _transition = true;
        _audio.clip = DayAmbient;
        _audio.Play();
    }

    private bool MatchDead(List<string> dead)
    {
        if(dead.Count != Dead.Count)
        {
            return false;
        }

        foreach(var d in Dead)
        {
            dead.Remove(d);
        }

        return !dead.Any();
    }

    public void SpawnAnimal(GameObject prefab)
    {
        var animal = GameObject.Instantiate(prefab);
        Sheeps.Add(animal.GetComponent<Sheep>());
        animal.transform.position = this.RandomFieldPoint();
    }

    public void ExecuteNight()
    {
        _audio.clip = NightAmbient;
        _audio.Play();

        if(!string.IsNullOrEmpty(_sacrificeName))
        {
            Dead.Add(_sacrificeName.ToLower());
        }

        var nightValue = Day == 1 ? 0 : Day + (_familyStart - Family.Count) * 2 + 2;

        var sacrifice = Family.FirstOrDefault(f => f.Name == _sacrificeName);

        while (nightValue > 0)
        {
            if (!Fence.Broken)
            {
                
                nightValue -= Fence.Value;
                if (nightValue > 0)
                {
                    Fence.Broken = true;
                }

                continue;
            }

            if (sacrifice != null)
            {
                nightValue -= 4;
                Family.Remove(sacrifice);
                _conditions[_sacrificeName.ToLower() + "killed"] = true;
                _conditions[_sacrificeName.ToLower() + "alive"] = false;
                Destroy(sacrifice.gameObject);
                sacrifice = null;
                _sacrificeName = "";
                SpawnAnimal(BlackSheep);
                continue;
            }

            var sheep = Sheeps.OrderBy(s => s.KillOrder).FirstOrDefault();

            if (sheep != null)
            {
                
                nightValue -= sheep.Value;
                if(nightValue > 0)
                {
                    Sheeps.Remove(sheep);
                    Destroy(sheep.gameObject);

                    if (RamPurchased > 0)
                    {
                        RamPurchased--;
                    }
                    else if (SheepPurchased > 0)
                    {
                        SheepPurchased--;
                    }
                }

                continue;
            }

            GameOver.State = GameOverState.GameOver;
            return;
        }

        var conv = ParseConversation(NightDialogs[Day - 1].text);
        DialogBox.ShowCoversation(conv.First(c => MatchDead(c.Dead)).Lines, EndNight);

        foreach (var sheep in Sheeps)
        {
            sheep.State = SheepState.Wool;
        }

        Day++;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Mother.Target != null)
        {
            Mother.Target.State = SheepState.Wool;
            Mother.Target.Wander();
            Mother.Target = null;
            Mother.State = MotherState.Idle;
        }
        Mother.GoTo(eventData.pointerPressRaycast.worldPosition);
    }

    private List<Conversation> ParseConversation(string raw)
    {
        var match = Regex.Match(raw, "\\[([^\\[]*)\\]([^\\[\\]]*)");
        var ret = new List<Conversation>();

        while(match.Success)
        {
            bool monster = false;
            var c = new Conversation();
            c.Dead = match.Groups[1].Value.Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(s => s.ToLower()).ToList();
            c.Lines = new List<ConversationEntry>();

            foreach (var line in match.Groups[2].Value.Split('\n').Where(s => !string.IsNullOrEmpty(s)))
            {
                if (line == "BadEndScreen")
                {
                    c.Lines.Last().GameOverState = GameOverState.BadEnd;
                    continue;
                }
                else if(line == "GoodEndScreen")
                {
                    c.Lines.Last().GameOverState = GameOverState.GoodEnd;
                    continue;
                }

                var split = line.Split('#');
                
                if(split[0] == "SOUNDCUE")
                {
                    continue;
                }
                else if(split[0] == "PAUSE")
                {
                    monster = true;
                    continue;
                }

                var entry = new ConversationEntry();
                entry.GameOverState = GameOverState.Hidden;
                entry.ShowMonster = monster;
                entry.Name = split[0];
                if (split.Length == 3)
                {
                    if (!GetCondition(split[1]))
                    {
                        continue;
                    }
                    entry.Line = split[2];
                }
                else if(split.Length == 2)
                {
                    entry.Name = split[0];
                    entry.Line = split[1];
                }
                else
                {
                    continue;
                }


                if (entry.Name == "Mother")
                {
                    entry.Portrait = Mother.Portrait;
                }
                else if(entry.Name == "Monster")
                {
                    entry.Portrait = Monster;
                }
                else
                {
                    try
                    {
                        entry.Portrait = Family.First(f => f.Name.ToLower() == entry.Name.ToLower()).CurrentPortrait;
                    }
                    catch
                    {

                    }
                    
                }

                c.Lines.Add(entry);
            }
            ret.Add(c);
            match = match.NextMatch();
        }
        return ret;
    }

    public class Conversation
    {
        public List<string> Dead { get; set; }

        public List<ConversationEntry> Lines {get;set;}
    }

    public class ConversationEntry
    {
        public GameOverState GameOverState { get; set; }

        public bool ShowMonster { get; set; }

        public string Name { get; set; }

        public string Line { get; set; }

        public Sprite Portrait { get; set; }
    }
}
